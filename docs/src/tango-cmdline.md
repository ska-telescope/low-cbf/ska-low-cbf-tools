# CommandLineDevice

The command to execute is provided as the Tango device property `Template`,
in the format of a Python template string. That is, `$` marks variables.
e.g.

    ./lfaa_sim -h $header_file -d $data_file -a $dest_ip -p $dest_port -r $repeats

Variables will be exposed as Tango attributes for clients to control.

Tango devices servers can be used without a Tango database, if desired, by
providing configuration in a text file (an example *emulator.txt* is provided).

To launch without a Tango database, use a command like the below. Note that port
number 8765 is only an example, an unused port number must be provided.

    python3 CommandLineDevice.py emulator -file=emulator.txt -ORBendPoint giop:tcp::8765

A Python Tango client can then control the command-line program, e.g.:

    import tango
    emulator = tango.DeviceProxy("tango://localhost:8765/low-cbf/test/emulator#dbase=no")
    print(emulator.args)
    emulator.header_file = "./header.raw"
    emulator.data_file = "./data.raw"
    emulator.dest_ip = "10.11.12.13"
    emulator.dest_port = "1234"
    emulator.repeats = "1"
    emulator.execute()

Execution is performed as a background task. Active PIDs are available via the
`pids` attribute.
