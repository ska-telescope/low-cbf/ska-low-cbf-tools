"""
A configurable command-line runner
"""
import re
import shlex
import subprocess
from string import Template


class ConfigurableCommand:
    """
    A configurable command-line runner.
    Arguments are exposed through array index notation.
    """

    def __init__(self, template: str):
        """
        :param template: Command and arguments as template string.
        e.g. "ls -la $path"
        """
        template = Template(template)

        # extract variable names from template string
        self._args = {
            var[1:]: ""  # empty string as default value
            for var in re.findall(
                re.escape(template.delimiter) + template.idpattern,
                template.template,
            )
        }
        """key: variable name (from template string)"""

        self._template = template
        self._processes = []
        self._stdout = ""
        self._stderr = ""

    def __setitem__(self, key: str, value: str):
        """
        Set an argument
        :param key: Variable name
        :param value: Value to set, will be shell escaped
        """
        # escape string to reduce potential for trickery
        # e.g. user inputting arguments like:
        # "my_file.txt; $(rm -rf important_file)"
        self._args[key] = shlex.quote(str(value))

    def __getitem__(self, key: str) -> str:
        """
        Get the current value of an argument
        """
        return self._args[key]

    @property
    def args(self) -> list:
        """
        Get the names of all arguments
        (i.e. variables in the template string)
        """
        return list(self._args.keys())

    @property
    def pids(self) -> list:
        """
        Get all active PIDs
        """
        self.__handle_processes()
        return [process.pid for process in self._processes]

    @property
    def stdout(self) -> str:
        """
        Get the output from the most recent execution of the command
        """
        self.__handle_processes()
        return self._stdout

    @property
    def stderr(self) -> str:
        """
        Get the error output from the most recent execution of the command
        """
        self.__handle_processes()
        return self._stderr

    def execute(self):
        """
        Execute the command using the current argument values
        """
        self._processes.append(
            subprocess.Popen(
                shlex.split(self._template.substitute(self._args)),
                shell=False,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                text=True,
            )
        )
        self.__handle_processes()

    def __handle_processes(self):
        """
        Capture stdout & remove defunct processes from our list
        """
        # we cache the new process list first, to ensure we don't lose any
        # before having a chance to collect their output.
        # poll() returns None if process is still alive
        active_processes = [_ for _ in self._processes if _.poll() is None]

        # capture output from last execution (if complete)
        if self._processes:
            last_process = self._processes[-1]
            if last_process not in active_processes:
                self._stdout = last_process.stdout.read()
                self._stderr = last_process.stderr.read()

        self._processes = active_processes
