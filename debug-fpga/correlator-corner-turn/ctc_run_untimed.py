# Script to setup FPGA to run Coarse Corner Turner (CTC) and run LFAA simulator
#  to send data to FPGA. Output from the Fine Filterbank after CTC can be
#  captured at the 25GbE debug port, and the captured packets compared against
#  output from the 'packetised model'
#
# Copyright (C) CSIRO 2020

import sys
import subprocess
#from random import randrange
#from threading import Thread
from time import sleep
from I_Gemini_Client import Client, ClientError, ServerErrorDecode, Peripheral, Slave, RegisterBlock, EventObserver, ARRAYCODE
from array import array



if __name__ == '__main__':

  fpga_ip = '10.32.0.4' # hbm dev board
#  fpga_ip = '10.32.0.3' # gemini card
  fpga = Client()
  fpga.trace = False
  fpga.timeoutUSecs = 10000
  fpga.clientMtu = 8000
  fpga.connect(fpga_ip)
  
  # Connect to the FPGA. Check that properties are working
  for i in range(2):
      if fpga.connected:
          break
      sleep(1)
  if not fpga.connected:
      print("Failed to connect to FPGA at {}".format(fpga_ip))
      sys.exit(1)

  # load all the registers that need to be set
  # (values to upload and registers to upload to specified in the file)
#  print('\nUploading FPGA register settings..')
#  upload ('test_setup.txt')
  print('\nInitialising read-out time\n')
  ctc_periph = Peripheral('ctcconfig')
  time_periph = Peripheral('timingcontrol')
  icnx_periph = Peripheral('interconnect')
  
  dbg_sel_reg = Slave(icnx_periph,'icstatctrl').regs('dbgdatasel','dbgdataselisoutput')
  dbg_sel_data = array(ARRAYCODE, [0x0 for i in range(0, dbg_sel_reg.len)])
  ctc_reset_reg = Slave(ctc_periph,'setup').regs('full_reset')
  reset_data = array(ARRAYCODE, [0x1 for i in range(0, ctc_reset_reg.len)])
  cycles_reg = Slave(ctc_periph, 'timing').regs('output_cycles')
  cycles_data = array(ARRAYCODE, [0x10cc for i in range(0, cycles_reg.len)])
  report_rst_reg = Slave(ctc_periph, 'report').regs('reset')
  report_rst_data = array(ARRAYCODE, [0x1 for i in range(0, report_rst_reg.len)])
  blks_rst_reg = Slave(ctc_periph, 'valid_blocks').regs('reset')
  blks_rst_data = array(ARRAYCODE, [0x1 for i in range(0, blks_rst_reg.len)])
  time_regs = Slave(time_periph, 'timing').regs('cur_time_seconds'
          , 'cur_time_ns')
  start_time_regs = Slave(ctc_periph, 'timing').regs(
          'starting_wall_time_seconds', 'starting_wall_time_nanos')
  time_data = array(ARRAYCODE, [0 for i in range(0,time_regs.len)])
  ctc_newcfg_reg = Slave(ctc_periph,'timing').regs('control_enable_timed_input',
          'control_enable_timed_output','control_use_new_config')
  newcfg_data = array(ARRAYCODE, [0x3 for i in range(0, ctc_newcfg_reg.len)])

# configure to capture Correlator fine filterbank/ fine delay output
  print('\n Cfg 25G to capture CTC Fine Filterbank output')
  # select interconnect input from CTC Fine Filterbank/delay
  dbg_sel_data[0] = 0x14
  dbg_sel_data[1] = 0x0
  fpga.write(dbg_sel_reg, dbg_sel_data)

# apply Reset
  print('CTC full reset applied')
  reset_data[0] = 0x1
  fpga.write(ctc_reset_reg, reset_data)

# Set number of output cycles
  print('CTC output cycles set')
  cycles_data[0] = 0x10cc
#  cycles_data[0] = 0x22cc
  fpga.write(cycles_reg, cycles_data)

# reset report
  print('CTC report cleared')
  report_rst_data[0] = 0x1
  fpga.write(report_rst_reg, report_rst_data)
  report_rst_data[0] = 0x0
  fpga.write(report_rst_reg, report_rst_data)
#  sleep(2)

# reset valid blocks
  print('CTC valid blocks reset')
  blks_rst_data[0] = 0x1
  fpga.write(blks_rst_reg, blks_rst_data)
  report_rst_data[0] = 0x0
  fpga.write(blks_rst_reg, blks_rst_data)

# Pulse new_cfg_register high briefly
  print('CTC start time load pulse applied')
  newcfg_data[0] = 0x1 
  fpga.write(ctc_newcfg_reg, newcfg_data)
  newcfg_data[0] = 0x0
  fpga.write(ctc_newcfg_reg, newcfg_data)
 
# release reset
  print('CTC full reset released')
  reset_data[0] = 0x0
  fpga.write(ctc_reset_reg, reset_data)
  
# read current time and program start time
#  print('CTC start time programmed')
#  fpga.read(time_regs, time_data)
#  time_data[0] += 3
#  fpga.write(start_time_regs, time_data)


  # Run LFAA simulator on mace2 to send packet data
  # (ou need to have set up passwordless ssh to mace2, or this fails)
  print('\nRunning LFAA simulator to send data...')
  subprocess.call(["ssh",
          "mace2",
          "lfaa/lfaa_sim -d lfaa/run5/LFAAData_fpga1.raw -h lfaa/run5/LFAAHeaders_fpga1.raw -a 192.168.1.99 -p30333" ])
#          "lfaa/lfaa_sim -d lfaa/run5/LFAAData_fpga1.raw -h lfaa/run5/LFAAHeaders_fpga1.raw -a 192.168.1.99 -p30333 -z 332000" ])

  # Download results from ram buffers in FPGA
  # (What to download specified in the results_list file)
#  print('\nDownloading results from FPGA registers..')
#  download('result_list.txt')

  # Compare data in each result files, with corresponding model file
#  print('\nComparing results files with model files ...')
#  compare_result_files('result_list.txt')

#  print('')
  sys.exit(0)

