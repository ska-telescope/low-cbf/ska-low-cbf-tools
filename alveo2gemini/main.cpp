/* A bridge program enabling Alveo cards to interact with Gemini Protocol, so
 * that registers in the Alveo can be accessed by Gemini clients over a network.
 *
 * The program interfaces Gemini Protocol to an OpenCL PCI implementation
 * by Xilinx for their Alveo cards
 *
 */
/* Copyright (c) 2020, CSIRO. */

#include <iostream>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <errno.h>
#include <string.h>
#include <cstring> // memset
#include <unistd.h> // getopt
#include <chrono>

#include "gemini.h"
#include "alveo_cl.h"

#define MAX_PKT_LEN 1500
#define PUBLISH_DEST_PORT 30001

volatile bool isSignalled = false;

void sigintHandler(int signum)
{
    std::cout << "Exiting..." << std::endl;
    isSignalled = true;
}

void setCtrlCHandler()
{
    struct sigaction act;
    act.sa_handler = sigintHandler;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
    sigaction(SIGINT, &act, NULL);
}

void usage(const char *progname)
{
    std::cout << "USAGE:" << std::endl;
    std::cout << progname <<  "[-b argsBaseAddr] [-f fpgaXclbinFilename] ";
    std::cout << "[-k kernelName] [-h hbm_config_string] [-p serverPort] "
              << "[-w] [-v]" << std::endl;
    return;
}


int main(int argc, char * argv[])
{
    // Default parameters
    uint16_t server_port = 30333;
    std::string fpga_bin_file("aug28.xclbin");
    std::string kernel_name("vitisAccelCore");
    std::string hbm_config("1Gi 1Gi");
    uint32_t args_shared_addr(0x8000);
    bool wait_4_debug = false;
    int device_num_to_use = 0;

    // Get command line options if any
    int ret;
    while((ret = getopt(argc, argv, "b:d:f:h:k:p:vw")) != -1)
    {
        switch(ret)
        {
            case 'b':
                args_shared_addr = std::stoul(optarg, 0, 0);
                break;
            case 'd':
                device_num_to_use = std::stol(optarg, 0, 10);
                break;
            case 'f':
                fpga_bin_file = std::string(optarg);
                break;
            case 'h':
                hbm_config = std::string(optarg);
                break;
            case 'k':
                kernel_name = std::string(optarg);
                break;
            case 'p':
                server_port = std::stoul(optarg, 0, 10) & 0x00ffff;
                break;
            case 'v':
                std::cout << "Last modified: " << LAST_MODIFIED_DATE
                        << "\nGit version  : " << GIT_VERSION << std::endl;
                return 0;
                break;
            case 'w':
                wait_4_debug = true;
                break;
            default:
                usage(argv[0]);
                return -1;
                break;
        }
    }

    // Initialise alveo card - load FPGA bitfile, allocate buffers
    Alveo_cl alveo_cl(device_num_to_use);
    int init_rslt = alveo_cl.init(fpga_bin_file, kernel_name, args_shared_addr
            , hbm_config, wait_4_debug);
    if(init_rslt != 0)
        return -1;

    // Install signal handler so we can be told to exit with ^C (SIGINT)
    setCtrlCHandler();

    // socket and packet to announce FPGA presence
    int pub_skt = socket(AF_INET, SOCK_DGRAM, 0);
    if(pub_skt == -1)
    {
        std::cout << strerror(errno) << std::endl;
        return -1;
    }
    int bcast_en = 1;
    setsockopt(pub_skt, SOL_SOCKET, SO_BROADCAST, &bcast_en, sizeof(bcast_en));

    // Bind publish socket to port above the server receive port
    struct sockaddr_in pub_skt_addr;
    memset(&pub_skt_addr, '\0', sizeof(pub_skt_addr));
    pub_skt_addr.sin_family = AF_INET;
    pub_skt_addr.sin_port = htons(server_port+1);
    pub_skt_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    int rv = bind(pub_skt, (struct sockaddr *)&pub_skt_addr
            , sizeof(pub_skt_addr));
    if(rv != 0)
    {
        std::cout << strerror(errno) << std::endl;
        return -1;
    }
    uint8_t pub_pkt [16] = {
              0x01, 0x80, 0x00, 0x00 // Gemini V1 packet, publish
            , 0x00, 0x00, 0x00, 0x01 // Event 1 -> Discovery
            , 0x00, 0x00, 0x00, 0x00 // Timestamp MS
            , 0x00, 0x00, 0x00, 0x00 // Timestamp LS
    };
    // Modify address to use when sending publish packets
    pub_skt_addr.sin_addr.s_addr = htonl(INADDR_BROADCAST);
    pub_skt_addr.sin_port = htons(PUBLISH_DEST_PORT);
    auto last_bcast_time = std::chrono::steady_clock::now();

    // Create socket that Gemini clients can connect to
    int skt = socket(AF_INET, SOCK_DGRAM, 0);
    if(skt == -1)
    {
        std::cout << strerror(errno) << std::endl;
        return -1;
    }
    struct sockaddr_in skt_addr;
    memset(&skt_addr, '\0', sizeof(skt_addr));
    skt_addr.sin_family = AF_INET;
    skt_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    skt_addr.sin_port = htons(server_port);
    rv = bind(skt, (struct sockaddr *)&skt_addr, sizeof(skt_addr));
    if(rv != 0)
    {
        std::cout << strerror(errno) << std::endl;
        return -1;
    }


    fd_set rx_fd;
    FD_ZERO(&rx_fd);
    FD_SET(skt, &rx_fd);
    int max_fd = skt;

    struct timeval wait_time;
    wait_time.tv_sec = 0;
    wait_time.tv_usec = 50000;

    // Function that Gemini Protocol processor calls to read regs via OpenCL
    auto reg_read_fn = [&alveo_cl] (uint32_t addr, int len,  char * buf)
    {
        alveo_cl.read(addr, (uint32_t *)buf, len);
    };
    // Function that Gemini Protocol processor calls to write regs via OpenCL
    auto reg_write_fn = [&alveo_cl] (uint32_t addr, int len,  char * buf)
    {
        alveo_cl.write(addr, (uint32_t *)buf, len);
    };

    // Gemini Protocol processor
    Gemini gemini_protocol(reg_write_fn, reg_read_fn);

    char rx_buf[MAX_PKT_LEN];
    struct sockaddr_in src_addr;
    memset(&src_addr, '\0', sizeof(src_addr));
    socklen_t src_addr_len = 0;

    // Event Loop forever receiving gemini packets unless signalled to exit
    while(!isSignalled)
    {
        fd_set read_set = rx_fd;
        struct timeval timeout = wait_time;
        int num_events = select(max_fd+1, &read_set, 0, 0, &timeout);
        if (num_events > 0)
        {
            // handle the events
            if(FD_ISSET(skt, &read_set))
            {
                // Read Gemini client input from socket
                src_addr_len = sizeof(src_addr);
                size_t rx_len = recvfrom(skt, rx_buf, sizeof(rx_buf), 0
                        , (struct sockaddr *) &src_addr, &src_addr_len);

                // Gemini Protocol processing of incoming packet
                size_t tx_len = sizeof(rx_buf);
                gemini_protocol.pkt_in(rx_buf, rx_len, &tx_len);

                // Send response back to client
                if(tx_len > 0)
                {
                    ssize_t nbytes = sendto(skt, rx_buf, tx_len, 0
                            , (struct sockaddr *) &src_addr, src_addr_len);
                    //std::cout << nbytes << " bytes reply" << std::endl;
                    if(nbytes < 0)
                    {
                        std::cout << "Couldn't send response packet"
                            << std::endl;
                        std::cout << "tx_len=" << tx_len
                            << " nbytes=" << nbytes
                            << "  sin_family=" << src_addr.sin_family
                            << std::endl;
                        std::cout << strerror(errno) << std::endl;
                    }
                }
            }
            else
            {
                std::cout << "event not in read-set" << std::endl;
            }
        }
        else if (num_events == 0)
        {
            ; // handle timeout
        }
        else
        {
            if(errno == EINTR)
                continue;
            std::cout << strerror(errno) << std::endl;
            return -1;
        }

        // Send a publish packet every two seconds
        auto time_now = std::chrono::steady_clock::now();
        auto elapsed_sec =  time_now - last_bcast_time;
        if(elapsed_sec > std::chrono::seconds(2))
        {
            last_bcast_time = time_now;
            ssize_t pub_rv = sendto(pub_skt, pub_pkt, sizeof(pub_pkt), 0
                , (struct sockaddr *) &pub_skt_addr, sizeof(pub_skt_addr));
            if(pub_rv == -1)
            {
                std::cout << "Failed to send Pub packet" << std::endl;
                std::cout << strerror(errno) << std::endl;
            }
        }

    }


    std::cout << "End." << std::endl;
    return 0;
}


